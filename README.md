# Architecture Ref. Cards

MIT License, Copyright (c) 2022, Berufsbildungsschule Winterthur, Rinaldo Lanza


# Übersicht
Die folgenden Beispiele/Apps dienen als Ausgangslage für die Erarbeitung möglicher Cloud Lösungen.
Die Beispiele sind sehr einfach gehalten und stellen exemplarische Anforderungen an die Laufzeitumgebung und
können im Unterricht erweitert werden.

Zu jeder Architektur und Ref. Card (Referenzkarte) existiert eine spezifische Dokumentation in den 
jeweiligen Git-Repositories.


## Architecture Ref. Card 01 / Spring Boot
Spring Boot Applikation als Website mit oder ohne API-Service. Z.B. als Website mit Statusinformation über das Netzwerk, Geo-Location, etc.

Anforderungen an die Cloud Lösung:
- Java Laufzeitumgebung
- Nginx als Webserver mit Weiterleitung an den internen Tomcat Port 8080
- Build Prozess mit Maven oder Gradle, Artefakt: JAR-Datei

Link zum Repository:<br/>
https://gitlab.com/bbwrl/m346-ref-card-01


## Architecture Ref. Card 02 / React
Einfache React-App ohne weitere Ressourcen. Die App könnte ausgebaut werden, mit der Anzeige 
bspw. Wetter-Daten oder einer anderen interessanten API.

Anforderungen an die Cloud Lösung:
- Simpler Webserver, z.B. Nginx
- Build Prozess mit Node/npm, Artefakt: Build Ordner mit HTML und JavaScript

Link zum Repository:<br/>
https://gitlab.com/bbwrl/m346-ref-card-02


## Architecture Ref. Card 03 / Spring Boot, Database
Spring Boot Applikation mit Datenbankanbindung, wie bspw. eine Witzedatenbank.		  	 

Anforderungen an die Cloud Lösung:
- Java Laufzeitumgebung
- Datenbanksystem wie MySQL, MariaDB, Postgres, etc.
- AWS oder Azure Datenbanksystem

Links zu den Repositories:<br/>
MariaDB: https://gitlab.com/bbwrl/m346-ref-card-03
<br/>
Azure SQL-Server: https://gitlab.com/bbwrl/m346-ref-card-03-azure


## Architecture Ref. Card 04 / Spring Boot, Storage
Spring Boot Applikation mit einer Fotogalerie und Upload der Fotos und Speicherung in Dateiablage.		  		 

Anforderungen an die Cloud Lösung:
- Java Laufzeitumgebung
- Storage System AWS..., Azure..., etc.

Links zu den Repositories:<br/>
localhost: https://gitlab.com/bbwrl/m346-ref-card-04
<br/>
Azure Storage: https://gitlab.com/bbwrl/m346-ref-card-04-azure
<br/>
AWS S3 Bucket: follows...

## Architecture Ref. Card 05 / React & Spring Boot
SOA mit React App als Frontend und Spring Boot Applikation als Backend, z.B. mit einer Witze-API als Mockup oder mit Statusinformation über das Netzwerk, Geo-Location, etc. (siehe Ref. Card 01 und 02)
Die beiden Beispiele beinhalten je eine Umgebungsvariable die gesetzt werden muss.

Anforderungen an die Cloud Lösung:
- Java Laufzeitumgebung für das Backend
- Simpler Webserver, z.B. Nginx für das Frontend

Links zu den Repositories:<br/>
Backend: https://gitlab.com/bbwrl/m346-ref-card-05-backend
<br/>
Frontend: https://gitlab.com/bbwrl/m346-ref-card-05-frontend

## Weiter Beispiele und Kombinationen
tbd.	

